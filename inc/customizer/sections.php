<?php
/**
 * Customizer sections.
 *
 * @package Sofia
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function sofia_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'sofia_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'sofia' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'sofia_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'sofia' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'sofia' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'sofia_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'sofia' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'sofia_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'sofia' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'sofia_customize_sections' );
